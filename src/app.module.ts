import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ClientsModule } from './clients/clients.module';
import { AuthController } from './auth/auth.controller';
import { AnthropometricModule } from './anthropometric/anthropometric.module';
import { MusclesModule } from './muscles/muscles.module';
import { ContraindicationsModule } from './contraindication/contraindication.module';
import { FilesModule } from './files/files.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    ClientsModule,
    AnthropometricModule,
    MusclesModule,
    ContraindicationsModule,
    FilesModule,

    ConfigModule.forRoot({
      load: [
        () => ({
          port: 8000,
        }),
      ],
    }),

    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'shoppingDB',
      entities: [__dirname + '/**/*.entity.{ts,js}'],
      synchronize: true,
    }),
  ],
  controllers: [AppController, AuthController],
  providers: [AppService],
})
export class AppModule {}
