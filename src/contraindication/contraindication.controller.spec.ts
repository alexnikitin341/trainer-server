import { Test, TestingModule } from '@nestjs/testing';
import { ContraindicationsController } from './contraindication.controller';

describe('ContraindicationsController', () => {
  let controller: ContraindicationsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ContraindicationsController],
    }).compile();

    controller = module.get<ContraindicationsController>(ContraindicationsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
