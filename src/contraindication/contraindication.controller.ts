import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiParam, ApiQuery } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { DeleteResult, UpdateResult } from 'typeorm';
import { ContraindicationEntity } from './contraindication.entity';
import { ContraindicationsService } from './contraindication.service';
import { CreateContraindicationDto } from './dto/create-contraindication.dto';
import { UpdateContraindicationDto } from './dto/update-contraindication.dto';

@ApiBearerAuth()
@ApiTags('Contraindications')
@Controller('Contraindications')
@UseGuards(JwtAuthGuard)
export class ContraindicationsController {
  constructor(private contraindicationsService: ContraindicationsService) {}

  @ApiQuery({
    name: 'text',
    required: false,
    type: String,
  })
  @Get()
  async GetAllByText(@Query('text') query: string): Promise<ContraindicationEntity[]> {
    return this.contraindicationsService.getAllByText(query);
  }

  @Post()
  async Create(@Body() muscle: CreateContraindicationDto): Promise<ContraindicationEntity> {
    return this.contraindicationsService.create(muscle);
  }

  @Post('csv')
  async CreateFromCSV(): Promise<string> {
    return this.contraindicationsService.createFromCSV();
  }

  @ApiParam({
    name: 'id',
    required: true,
    description: 'muscle identifier',
  })
  @Get(':id')
  async GetOne(@Param() { id }: { id: string }): Promise<ContraindicationEntity> {
    return await this.contraindicationsService.getById(id);
  }

  @ApiParam({
    name: 'id',
    required: true,
    description: 'muscle identifier',
  })
  @Put(':id')
  async Update(
    @Param() { id }: { id: string },
    @Body() product: UpdateContraindicationDto,
    @Request() req,
  ): Promise<UpdateResult> {
    return await this.contraindicationsService.update(id, product);
  }

  @ApiParam({
    name: 'id',
    required: true,
    description: 'muscle identifier',
  })
  @Delete(':id')
  async Delete(
    @Param() { id }: { id: string },
    @Request() req,
  ): Promise<DeleteResult> {
    return await this.contraindicationsService.delete(id);
  }
}
