import { ApiProperty } from '@nestjs/swagger';

export class UpdateContraindicationDto {
  @ApiProperty({
    nullable: true,
    description: 'Название противопоказания',
    example: 'гипертония',
  })
  name!: string;

  @ApiProperty({
    nullable: true,
    description: 'Название противопоказания на англ',
    example: 'hypertension',
  })
  nameEng?: string;

  @ApiProperty({
    description: 'описание',
    example:
      'или повышенное кровяное давление — серьезное патологическое состояние, значительно повышающее риск развития заболеваний сердечно-сосудистой системы, головного мозга, почек и других болезней',
  })
  description?: string;
}
