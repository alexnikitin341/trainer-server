import { Module } from '@nestjs/common';
import { CsvModule } from 'nest-csv-parser';
import { ContraindicationsService } from './contraindication.service';
import { ContraindicationsController } from './contraindication.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContraindicationEntity } from './contraindication.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ContraindicationEntity]), CsvModule],
  providers: [ContraindicationsService],
  controllers: [ContraindicationsController],
})
export class ContraindicationsModule {}
