import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { createReadStream } from 'fs';
import { CsvParser } from 'nest-csv-parser';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { ContraindicationEntity } from './contraindication.entity';
import { CreateContraindicationDto } from './dto/create-contraindication.dto';
import { UpdateContraindicationDto } from './dto/update-contraindication.dto';

class ContraindicationsTableHeaders {
  name: string;
  description: string;
}

@Injectable()
export class ContraindicationsService {
  constructor(
    @InjectRepository(ContraindicationEntity)
    private contraindicationRepository: Repository<ContraindicationEntity>,
    private readonly csvParser: CsvParser,
  ) {}

  async getAllByText(text: string): Promise<ContraindicationEntity[]> {
    return await this.contraindicationRepository
      .createQueryBuilder('contraindication')
      .where('contraindication.name like :name', { name: '%' + text + '%' })
      .getMany();
  }

  async getById(id: string): Promise<ContraindicationEntity> {
    return await this.contraindicationRepository.findOneBy({ id });
  }

  async create(
    client: CreateContraindicationDto,
  ): Promise<ContraindicationEntity> {
    return await this.contraindicationRepository.save(client);
  }

  async update(
    id: string,
    product: UpdateContraindicationDto,
  ): Promise<UpdateResult> {
    return await this.contraindicationRepository.update(id, product);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.contraindicationRepository.delete(id);
  }

  async createFromCSV(): Promise<string> {
    const stream = createReadStream('files/contraindications.csv');
    const { list } = await this.csvParser.parse(
      stream,
      ContraindicationsTableHeaders,
      0,
      0,
      { separator: ',' },
    );

    list.forEach(async (item) => {
      await this.create({ name: item.name });
    });

    return 'success';
  }
}
