import { Test, TestingModule } from '@nestjs/testing';
import { ContraindicationsService } from './contraindication.service';

describe('ContraindicationsService', () => {
  let service: ContraindicationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ContraindicationsService],
    }).compile();

    service = module.get<ContraindicationsService>(ContraindicationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
