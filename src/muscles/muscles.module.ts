import { Module } from '@nestjs/common';
import { CsvModule } from 'nest-csv-parser';
import { MusclesService } from './muscles.service';
import { MusclesController } from './muscles.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MuscleEntity } from './muscle.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MuscleEntity]), CsvModule],
  providers: [MusclesService],
  controllers: [MusclesController],
})
export class MusclesModule {}
