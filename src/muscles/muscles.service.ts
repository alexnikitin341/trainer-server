import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { createReadStream, readFileSync } from 'fs';
import { CsvParser } from 'nest-csv-parser';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateMuscleDto } from './dto/create-muscle.dto';
import { UpdateMuscleDto } from './dto/update-muscle.dto';
import { MuscleEntity } from './muscle.entity';

class MusclesTableHeaders {
  name: string;
  start: string;
  attaching: string;
  function: string;
}

@Injectable()
export class MusclesService {
  constructor(
    @InjectRepository(MuscleEntity)
    private muscleRepository: Repository<MuscleEntity>,
    private readonly csvParser: CsvParser,
  ) {}

  async getAllByText(text: string): Promise<MuscleEntity[]> {
    return await this.muscleRepository
      .createQueryBuilder('muscle')
      .where('muscle.name like :name', { name: '%' + text + '%' })
      .getMany();
  }

  async getById(id: string): Promise<MuscleEntity> {
    return await this.muscleRepository.findOneBy({ id });
  }

  async create(client: CreateMuscleDto): Promise<MuscleEntity> {
    return await this.muscleRepository.save(client);
  }

  async update(id: string, product: UpdateMuscleDto): Promise<UpdateResult> {
    return await this.muscleRepository.update(id, product);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.muscleRepository.delete(id);
  }

  async createFromCSV(): Promise<string> {
    const stream = createReadStream('files/muscles.csv');
    const { list } = await this.csvParser.parse(
      stream,
      MusclesTableHeaders,
      0,
      0,
      { separator: ',' },
    );

    list.forEach(async (muscle) => {
      await this.create({ name: muscle.name, function: muscle.function });
    });

    return 'success';
  }
}
