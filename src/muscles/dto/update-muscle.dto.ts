import { ApiProperty } from '@nestjs/swagger';

export class UpdateMuscleDto {
  @ApiProperty({
    nullable: true,
    description: 'Название мышцы',
    example: 'Лобная мышца',
  })
  name?: string;

  @ApiProperty({
    nullable: true,
    description: 'Название мышцы на англ',
    example: 'm.frontalis',
  })
  nameEng?: string;

  @ApiProperty({
    description: 'функция мышцы',
    example:
      'смещение кожи волосистой части головы кпереди – горизонтальные складки на лбу, поднимает брови',
  })
  function?: string;
}
