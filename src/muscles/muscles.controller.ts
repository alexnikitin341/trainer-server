import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiParam, ApiQuery } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CreateMuscleDto } from './dto/create-muscle.dto';
import { UpdateMuscleDto } from './dto/update-muscle.dto';
import { MuscleEntity } from './muscle.entity';
import { MusclesService } from './muscles.service';

@ApiBearerAuth()
@ApiTags('Muscles')
@Controller('muscles')
@UseGuards(JwtAuthGuard)
export class MusclesController {
  constructor(private musclesService: MusclesService) {}

  @ApiQuery({
    name: 'text',
    required: false,
    type: String,
  })
  @Get()
  async GetAllByText(@Query('text') query: string): Promise<MuscleEntity[]> {
    return this.musclesService.getAllByText(query);
  }

  @Post()
  async Create(@Body() muscle: CreateMuscleDto): Promise<MuscleEntity> {
    return this.musclesService.create(muscle);
  }

  @Post('csv')
  async CreateFromCSV(): Promise<string> {
    return this.musclesService.createFromCSV();
  }

  @ApiParam({
    name: 'id',
    required: true,
    description: 'muscle identifier',
  })
  @Get(':id')
  async GetOne(@Param() { id }: { id: string }): Promise<MuscleEntity> {
    return await this.musclesService.getById(id);
  }

  @ApiParam({
    name: 'id',
    required: true,
    description: 'muscle identifier',
  })
  @Put(':id')
  async Update(
    @Param() { id }: { id: string },
    @Body() product: UpdateMuscleDto,
    @Request() req,
  ): Promise<UpdateResult> {
    return await this.musclesService.update(id, product);
  }

  @ApiParam({
    name: 'id',
    required: true,
    description: 'muscle identifier',
  })
  @Delete(':id')
  async Delete(
    @Param() { id }: { id: string },
    @Request() req,
  ): Promise<DeleteResult> {
    return await this.musclesService.delete(id);
  }
}
