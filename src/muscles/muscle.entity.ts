import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class MuscleEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({
    default: '',
  })
  name: string;

  @Column({
    default: '',
  })
  nameEng?: string;

  @Column({
    default: '',
  })
  function?: string;
}
