import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { DeleteResult, UpdateResult } from 'typeorm';
import { ClientEntity } from './client.entity';
import { ClientsService } from './clients.service';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';

@ApiBearerAuth()
@ApiTags('Client')
@Controller('client')
@UseGuards(JwtAuthGuard)
export class ClientsController {
  constructor(private clientsService: ClientsService) {}

  @Get()
  async GetAll(): Promise<ClientEntity[]> {
    return this.clientsService.getAll();
  }

  @Post()
  async Create(@Body() client: CreateClientDto): Promise<ClientEntity> {
    return this.clientsService.create(client);
  }

  @ApiParam({
    name: 'clientId',
    required: true,
    description: 'client identifier',
  })
  @Get(':clientId')
  async GetOne(
    @Param() { clientId }: { clientId: string },
  ): Promise<ClientEntity> {
    return await this.clientsService.getById(clientId);
  }

  @ApiParam({
    name: 'clientId',
    required: true,
    description: 'client identifier',
  })
  @Put(':clientId')
  async Update(
    @Param() { clientId }: { clientId: string },
    @Body() product: UpdateClientDto,
    @Request() req,
  ): Promise<UpdateResult> {
    return await this.clientsService.update(clientId, product);
  }

  @ApiParam({
    name: 'clientId',
    required: true,
    description: 'client identifier',
  })
  @Delete(':clientId')
  async Delete(
    @Param() { clientId }: { clientId: string },
    @Request() req,
  ): Promise<DeleteResult> {
    return await this.clientsService.delete(clientId);
  }
}
