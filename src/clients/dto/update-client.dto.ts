import { ApiProperty } from '@nestjs/swagger';
import { Gender } from '../client.interface';

export class UpdateClientDto {
  @ApiProperty({
    description: 'номер телефона',
    example: '89198900000',
  })
  phone: string;

  @ApiProperty({ description: 'фамилия', example: 'Иванов' })
  surname: string;

  @ApiProperty({ description: 'имя', example: 'Иван' })
  name: string;

  @ApiProperty({ description: 'отчество', example: 'Иванович' })
  patronymic: string;

  @ApiProperty({ description: 'цель визита', example: 'улучшить свою форму' })
  visitTarget: string;

  @ApiProperty({ description: 'пол', example: 'man' })
  gender: Gender;

  @ApiProperty({
    description: 'дата рождения',
    example: '1996-11-24T14:22:39.000Z"',
  })
  birthDate: string;
}
