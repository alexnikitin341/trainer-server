import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { ClientEntity } from './client.entity';
import { AnthropometricEntity } from 'src/anthropometric/anthropometric.entity';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(ClientEntity)
    private clientRepository: Repository<ClientEntity>,
  ) {}

  async getAll(): Promise<ClientEntity[]> {
    return await this.clientRepository.find();
  }

  async getById(id: string): Promise<ClientEntity> {
    const client = await this.clientRepository.findOne({
      where: { id },
      relations: { anthropometrics: true },
    });

    if (!client) {
      throw new NotFoundException('The client was not found');
    }

    const allAnthropometricsClient = client.anthropometrics;

    const anthropometricsByType = allAnthropometricsClient.reduce<{
      [key: string]: AnthropometricEntity[];
    }>((acc, antr) => {
      acc[antr.type] = [...(acc[antr.type] || []), antr];

      return acc;
    }, {});

    const sortedByDateAnthropometrics = Object.entries(
      anthropometricsByType,
    ).reduce((acc, [type, anthrs]) => {
      return {
        ...acc,
        [type]: anthrs.sort(
          (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime(),
        ),
      };
    }, {});

    return { ...client, ...sortedByDateAnthropometrics };
  }

  async create(client: CreateClientDto): Promise<ClientEntity> {
    return await this.clientRepository.save(client);
  }

  async update(id: string, product: UpdateClientDto): Promise<UpdateResult> {
    return await this.clientRepository.update(id, product);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.clientRepository.delete(id);
  }
}
