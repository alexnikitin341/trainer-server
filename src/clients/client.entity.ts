import { AnthropometricEntity } from 'src/anthropometric/anthropometric.entity';
import {
  Entity,
  JoinColumn,
  OneToMany,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Gender } from './client.interface';

@Entity()
export class ClientEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({
    unique: true,
  })
  phone!: string;

  @Column()
  surname: string;

  @Column({
    default: '',
  })
  name?: string;

  @Column({
    default: '',
  })
  patronymic?: string;

  @Column({
    default: '',
  })
  visitTarget?: string;

  @Column({
    default: null,
  })
  birthDate?: Date | null;

  @Column({
    default: Gender.man,
  })
  gender: Gender;

  @CreateDateColumn()
  createdAt: String;

  @UpdateDateColumn()
  updtedAt: String;

  @OneToMany(
    () => AnthropometricEntity,
    (anthropometric) => anthropometric.client,
  )
  anthropometrics: AnthropometricEntity[];
}
