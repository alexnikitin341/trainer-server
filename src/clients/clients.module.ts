import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnthropometricEntity } from 'src/anthropometric/anthropometric.entity';
import { AnthropometricService } from 'src/anthropometric/anthropometric.service';
import { ClientsService } from './clients.service';
import { ClientsController } from './clients.controller';
import { ClientEntity } from './client.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ClientEntity, AnthropometricEntity])],
  controllers: [ClientsController],
  providers: [ClientsService, AnthropometricService],
})
export class ClientsModule {}
