import {
  Controller,
  Get,
  Param,
  Post,
  Res,
  UploadedFiles,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { FilesService } from './files.service';
import { ApiFiles } from './api-files.decorator';
import { join } from 'path';
import { Response } from 'express';

@ApiBearerAuth()
@ApiTags('Files')
@Controller('files')
@UseGuards(JwtAuthGuard)
export class FilesController {
  constructor(private filesService: FilesService) {}

  @ApiParam({
    name: 'name',
    description: 'name',
  })
  @ApiResponse({ type: Buffer })
  @Get(':name')
  async GetByName(@Param('name') name: string, @Res() res: Response) {
    const path = join(process.cwd(), `uploads/${name}`);
    res.sendFile(path);
    return;
  }

  @Post()
  @ApiFiles('files', true)
  @ApiConsumes('multipart/form-data')
  async post(@UploadedFiles() files: Express.Multer.File): Promise<void> {}
}
