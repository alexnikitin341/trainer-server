import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class FilesEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({
    default: '',
  })
  name: string;
}
