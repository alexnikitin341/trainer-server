import { ClientEntity } from 'src/clients/client.entity';
import {
  Entity,
  JoinColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id!: string;

  @Column({ unique: true })
  email!: string;

  @Column()
  surname: string;

  @Column()
  name?: string;

  @Column({
    default: '',
  })
  patronymic?: string;

  @Column()
  password: string;

  @Column({
    default: '',
  })
  role: string;

  @CreateDateColumn()
  createdAt: String;

  @UpdateDateColumn()
  updtedAt: String;

  @CreateDateColumn({
    default: null,
  })
  birthDate?: Date;

  @Column({
    default: null,
  })
  phone?: string;

  @OneToMany((type) => ClientEntity, ({ id }) => id)
  @JoinColumn()
  clients: ClientEntity[];
}
