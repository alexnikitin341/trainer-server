export enum AnthropometricTypes {
  weight = 'weight',
  height = 'height',
  limbLength = 'limbLength',
  fatPercentage = 'fatPercentage',
  mainIndicators = 'mainIndicators',
  kofCoolness = 'kofCoolness',
  caliper = 'caliper',
}
