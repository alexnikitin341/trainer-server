import { Test, TestingModule } from '@nestjs/testing';
import { AnthropometricService } from './anthropometric.service';

describe('AnthropometricService', () => {
  let service: AnthropometricService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnthropometricService],
    }).compile();

    service = module.get<AnthropometricService>(AnthropometricService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
