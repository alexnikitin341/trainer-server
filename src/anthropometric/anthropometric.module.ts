import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientEntity } from 'src/clients/client.entity';
import { ClientsService } from 'src/clients/clients.service';
import { AnthropometricController } from './anthropometric.controller';
import { AnthropometricEntity } from './anthropometric.entity';
import { AnthropometricService } from './anthropometric.service';

@Module({
  imports: [TypeOrmModule.forFeature([AnthropometricEntity, ClientEntity])],
  controllers: [AnthropometricController],
  providers: [AnthropometricService, ClientsService],
})
export class AnthropometricModule {}
