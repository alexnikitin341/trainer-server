import { Test, TestingModule } from '@nestjs/testing';
import { AnthropometricController } from './anthropometric.controller';

describe('AnthropometricController', () => {
  let controller: AnthropometricController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AnthropometricController],
    }).compile();

    controller = module.get<AnthropometricController>(AnthropometricController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
