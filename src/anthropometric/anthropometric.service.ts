import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { ClientsService } from 'src/clients/clients.service';
import { AnthropometricEntity } from './anthropometric.entity';
import { UpdateAnthropometricDto } from './dto/update-anthropometric.dto';
import { CreateAnthropometricDto } from './dto/create-anthropometric.dto';

@Injectable()
export class AnthropometricService {
  constructor(
    @InjectRepository(AnthropometricEntity)
    private anthropometricRepository: Repository<AnthropometricEntity>,
    private clientsService: ClientsService,
  ) {}

  async create({
    clientId,
    value,
    symbol,
    type,
    date,
  }: CreateAnthropometricDto): Promise<any> {
    const client = await this.clientsService.getById(clientId);

    if (!client) {
      throw new NotFoundException('The client was not found');
    }

    const savedAnthropometric = await this.anthropometricRepository.save({
      value,
      symbol,
      type,
      date,
      client,
    });

    return savedAnthropometric;
  }

  async update(
    id: string,
    product: UpdateAnthropometricDto,
  ): Promise<UpdateResult> {
    return await this.anthropometricRepository.update(id, product);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.anthropometricRepository.delete(id);
  }

  async getAll(): Promise<AnthropometricEntity[]> {
    return await this.anthropometricRepository.find();
  }

  async getsAnthropometricsByCliendId(
    clientId: string,
  ): Promise<AnthropometricEntity[]> {
    const allAnthropometrics = await this.anthropometricRepository.find({
      relations: { client: true },
      where: { client: { id: clientId } },
    });
    return allAnthropometrics.filter(
      (anthropometric) => anthropometric.client.id === clientId,
    );
  }

  async getByAnthropometricId(id: string): Promise<AnthropometricEntity> {
    return await this.anthropometricRepository.findOneBy({ id });
  }
}
