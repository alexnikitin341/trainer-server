import { AnthropometricEntity } from './anthropometric.entity';

describe('AnthropometricEntity', () => {
  it('should be defined', () => {
    expect(new AnthropometricEntity()).toBeDefined();
  });
});
