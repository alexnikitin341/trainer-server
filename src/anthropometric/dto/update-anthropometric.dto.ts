import { ApiProperty } from '@nestjs/swagger';
import { AnthropometricTypes } from '../anthropometric.interfaces';

export class UpdateAnthropometricDto {
  @ApiProperty({
    description: 'дата замера',
    example: '2022-10-17T10:38:17.000Z',
  })
  date: string;

  @ApiProperty({ description: 'значение измерения', example: 10 })
  value: number;

  @ApiProperty({ description: 'в чем измеряется', example: 'кг' })
  symbol: string;

  @ApiProperty({ description: 'тип измерения', example: 'weight' })
  type: AnthropometricTypes;
}
