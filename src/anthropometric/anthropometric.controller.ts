import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AnthropometricEntity } from './anthropometric.entity';
import { AnthropometricService } from './anthropometric.service';
import { CreateAnthropometricDto } from './dto/create-anthropometric.dto';
import { UpdateAnthropometricDto } from './dto/update-anthropometric.dto';

@ApiBearerAuth()
@ApiTags('Anthropometric')
@UseGuards(JwtAuthGuard)
@Controller('anthropometric')
export class AnthropometricController {
  constructor(private anthropometricService: AnthropometricService) {}

  @Post()
  async Create(
    @Body()
    data: CreateAnthropometricDto,
  ): Promise<AnthropometricEntity> {
    return this.anthropometricService.create(data);
  }

  @Get()
  async GetAll(): Promise<AnthropometricEntity[]> {
    return this.anthropometricService.getAll();
  }

  @ApiParam({
    name: 'anthropometricId',
    required: true,
    description: 'anthropometric identifier',
  })
  @Get(':anthropometricId')
  async GetByCliendId(
    @Param() { anthropometricId }: { anthropometricId: string },
  ): Promise<AnthropometricEntity> {
    return this.anthropometricService.getByAnthropometricId(anthropometricId);
  }

  @ApiParam({
    name: 'anthropometricId',
    required: true,
    description: 'anthropometric identifier',
  })
  @Put(':anthropometricId')
  async Update(
    @Param() { anthropometricId }: { anthropometricId: string },
    @Body() product: UpdateAnthropometricDto,
  ): Promise<UpdateResult> {
    return await this.anthropometricService.update(anthropometricId, product);
  }

  @ApiParam({
    name: 'anthropometricId',
    required: true,
    description: 'anthropometric identifier',
  })
  @Delete(':anthropometricId')
  async Delete(
    @Param() { anthropometricId }: { anthropometricId: string },
  ): Promise<DeleteResult> {
    return await this.anthropometricService.delete(anthropometricId);
  }
}
