import { ClientEntity } from 'src/clients/client.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { AnthropometricTypes } from './anthropometric.interfaces';

@Entity()
export class AnthropometricEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(() => ClientEntity, (client) => client.anthropometrics)
  client: ClientEntity;

  @Column()
  value: number;

  @Column()
  symbol: string;

  @Column()
  type: AnthropometricTypes;

  @Column({
    default: new Date().toISOString(),
  })
  date: string;

  @CreateDateColumn()
  createdAt: String;

  @UpdateDateColumn()
  updtedAt: String;
}
