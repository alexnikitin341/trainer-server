import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiProperty({
    example: '111',
    description: 'User@mail.com',
  })
  email: string;

  @ApiProperty({
    example: '222',
    description: '0000',
  })
  password: string;
}
